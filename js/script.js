const addElement = () => {
    let currentValue = $(".task-description").val();
    let currentPriority = $(".task-select :selected").val();
    $(".list-of-tasks").append("<li class='" + currentPriority + "'><input type='checkbox' class='task-checkbox' >" + currentValue + "</li>");
    $(".task-description").val('');
};


$(".task-description").keyup(function(event){
    if(event.keyCode == 13){
        $(".task-button").click();
    }
});

$(document).ready(function() {
    $(".list-of-tasks").on('change','.task-checkbox',function () {
        if($(this).is(':checked')) {
            $(this).parent().remove();
        }
        console.log('task deleted');
    });
});

